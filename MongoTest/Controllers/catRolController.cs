﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Configuration;

namespace MongoTest.Controllers
{
    public class catRolController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(string _id)
        {
            var connection = WebConfigurationManager.ConnectionStrings["MongoDB"].ToString();
            MongoDataService dataService = new MongoDataService(connection);
            string query = "{ _id: " + _id + " }";
            return dataService.findOne("pruebas", "catRol", query);
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}