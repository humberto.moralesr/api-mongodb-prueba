﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver;
using MongoDB.Bson;

namespace MongoTest
{
    public class MongoDataService
    {
         private MongoServer server;
            private string database { get; set; }
            public MongoDataService(string connectionString)
            {
                MongoClient client = new MongoClient(connectionString);
                server = client.GetServer();
            }
            
            public string findOne(string databaseName, string collectionName, string query)
            {
                var db = server.GetDatabase(databaseName);
                var collection = db.GetCollection(collectionName);
                BsonDocument bsonDoc = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(query);

                try
                {
                    var result = collection.FindOne(new QueryDocument(bsonDoc));

                    if (result != null)
                    {
                        return result.ToJson();
                    }
                    else
                    {
                        return "";
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
                
            }
    }
}